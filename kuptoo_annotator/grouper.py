import numpy as np
from collections import defaultdict
from kuptoo_annotator.encoders.tags import Tags


class Annotations(dict):

    def to_text_annotations(self):
        return {k: v for k, v in self.as_text()}

    def as_text(self):
        for k, v in self.items():
            yield k, [_v.text for _v in v]

    def text_with_probs(self):
        for k, v in self.items():
            yield k, [(_v.text, [__v[0].prob for __v in _v.probs]) for _v in v]

    def text_with_probs_list(self):
        for k, v in self.items():
            yield k, [(_v.text, [[(___v.tag, ___v.prob) for ___v in __v] for __v in _v.probs]) for _v in v]

    def text_with_prob(self):
        for k, v in self.items():
            yield k, [(_v.text, _v.probs[0][0].prob) for _v in v]


class TagProb:

    def __init__(self, tag, prob, continuous):
        self.continuous = continuous
        self.prob = prob
        self.tag = tag

    def __repr__(self):
        return "TagProb(tag={}, prob={}, continuous={})".format(self.tag, self.prob, self.continuous)


class Chunk:
    def __init__(self, text, probs):
        self.probs = probs
        self.text = text

    def __str__(self):
        return self.text

    def __repr__(self):
        return "Chunk(text={}, probs={})".format(self.text, self.probs)


class Grouper:

    def __init__(self, prediction, max_words, remapping: dict = None):
        self.max_words = max_words
        self.remapping = remapping
        self.tags = Tags()
        self.vectors = self._preprocess_vectors(prediction)
        self.annotations = defaultdict(list)
        self.prefixes = ['do', 'pod', 'na', 'z', 'w', 'dla', 'ze', 'we', 'any', 'bez', 'od', 'przeciw', 'od']

    def _preprocess_vectors(self, sentence_vectors):
        result = []
        for one_hot_vector in sentence_vectors:
            max_result = np.argmax(one_hot_vector)
            one_hot_vector = [
                TagProb(self.tags.idx2tag[word_position].name, float(probability), self.tags.idx2tag[word_position].continuous)
                for word_position, probability in enumerate(one_hot_vector)]

            one_hot_vector = sorted(one_hot_vector, reverse=True, key=lambda x: x.prob)

            result.append(
                (self.tags.idx2tag[max_result].name, self.tags.idx2tag[max_result].continuous, one_hot_vector[:3])
            )
        return result

    def _append_annotation(self, group, tag, group_probs):
        value = " ".join(group)

        if self.remapping and tag in self.remapping:
            tag = self.remapping[tag]

        if value not in self.annotations[tag]:
            self.annotations[tag].append(Chunk(value, group_probs))

    def group(self, text, filter_tags=None, allowed_tags=None) -> Annotations:

        sentence = text.split()[: self.max_words]
        group = []
        group_probs = []
        max_sentence_position = self.max_words - 1
        for word_position, word in enumerate(sentence):

            tag = self.vectors[word_position][0]
            probs = self.vectors[word_position][2]

            if tag == self.tags.idx2tag[0].name and word in self.prefixes:
                continue

            if tag in ['<pad>', 'unknown']:
                continue

            if filter_tags is not None and tag in filter_tags:
                continue

            if allowed_tags is not None and tag not in allowed_tags:
                continue

            group.append(word)
            group_probs.append(probs)

            if max_sentence_position == word_position:
                # to jest ostatnia pozycja w zdaniu
                self._append_annotation(group, tag, group_probs)
                group = []
                group_probs = []
            elif tag != self.vectors[word_position + 1][0]:
                # zmienił się tag
                self._append_annotation(group, tag, group_probs)
                group = []
                group_probs = []
            elif not self.vectors[word_position + 1][1]:
                # kolejna pozycja w zdaniu nie jest ciągła
                self._append_annotation(group, tag, group_probs)
                group = []
                group_probs = []

        return Annotations(dict(self.annotations))
