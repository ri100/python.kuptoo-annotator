import os
import tensorflow as tf
from data_utils.encoder_list import EncoderList
from data_utils.encoders.dbs.word2measure import Word2Measure
from data_utils.encoders.word2vec_from_dict_encoder import Word2VectorFromDictEncoder
from data_utils.nlp.standardizers import StandardizedText
from kuptoo_annotator.grouper import Grouper
from kuptoo_annotator.model.w2v import word2vec_with_pos

local_dir = os.path.dirname(__file__)


class TaggerModel:
    def __init__(self, model_path: str = None, remapping: dict = None):
        max_words_in_title = 16
        word2vec_with_pos_dim = 312
        if model_path is None:
            model_path = os.path.join(
                local_dir,
                "../data/dataset-20/model/sunny-darkness-1.e-114.vl-0.4073.l-0.40961262.h5")

        self.model = tf.keras.models.load_model(model_path)
        if remapping is None:
            remapping = {}
        self.remapping = remapping
        word2measure = Word2Measure(20, skip_if_in=word2vec_with_pos)
        self.encoders = EncoderList({
            'input_w2v': Word2VectorFromDictEncoder(max_words_in_title, word2vec_with_pos, word2vec_with_pos_dim),
            'input_aux': Word2VectorFromDictEncoder(max_words_in_title, word2measure, word2measure.dim),
        })

    def summary(self):
        print(self.model.summary())

    def predict(self, texts, filter_tags=None, allowed_tags=None):

        def _tokenized(text):
            return " ".join(StandardizedText(text).tokenize(trim_punctuation=True))

        def _predict(texts):
            x = self.encode_input(texts)
            predictions = self.model.predict(x)
            words, _ = predictions[0].shape
            for position, text in enumerate(texts):
                yield Grouper(predictions[position], words, self.remapping).group(text, filter_tags, allowed_tags)

        texts = [_tokenized(text) for text in texts]
        tags = list(_predict(texts))

        tags = [list(tag.text_with_prob()) for tag in tags]
        tags = [{k: v for k, v in tag} for tag in tags]

        return tags

    def encode_input(self, texts):
        return self.encoders.batch_encode(texts)
