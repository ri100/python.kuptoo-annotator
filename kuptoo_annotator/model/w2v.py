import os

from kuptoo_annotator.utils.serializer import load_obj

word2vec_with_pos = load_obj(
    os.path.join(
        os.path.dirname(__file__),
        "../data/dataset-20/embeddings/pos_w2v.pickle")
)
