from annotator_dbs.storage import lazy_rocks_dbs
from annotator_enums.enums import Key

from kuptoo_annotator.grouper import Annotations


def stem(tags):
    stem_dict = {}

    def _get_tags(tags):
        if isinstance(tags, Annotations):
            for r, w in tags.as_text():
                yield r, w
        else:
            for r, w in tags.items():
                yield r, w

    stem = lazy_rocks_dbs[Key.STEM]
    for rel, words in _get_tags(tags):
        if rel in ['adj',
                   Key.REL_ANTY,
                   Key.REL_BEZ,
                   Key.REL_DO,
                   Key.REL_DLA,
                   Key.REL_NA,
                   Key.REL_NAD,
                   Key.REL_OD,
                   Key.REL_O,
                   Key.REL_POD,
                   Key.REL_PO,
                   Key.REL_PRZECIW,
                   Key.REL_PRZED,
                   Key.REL_PRZEZ,
                   Key.REL_Z,
                   Key.REL_W,
                   Key.REL_ZE,
                   Key.REL_ZA,
                   ]:

            stemed_list = []
            for word in words:
                stemed_word_list = stem[word]
                if stemed_word_list is not None:
                    for stemed in stemed_word_list:
                        stemed_list.append(stemed)
                else:
                    stemed_list.append(word)
            stem_dict[rel] = stemed_list

    return stem_dict
