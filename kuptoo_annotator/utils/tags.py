from annotator_enums.enums import Key

basic_tag_keys = ['products_1', Key.WYTWORY, Key.MARKETING, Key.MARKA, 'adj', 'products_2', Key.REL_ANTY, Key.REL_BEZ,
                  Key.REL_DLA, Key.REL_DO, Key.REL_NA, Key.REL_NAD, Key.REL_OD, Key.REL_O, Key.REL_POD, Key.REL_PO,
                  Key.REL_PRZECIW, Key.REL_PRZED, Key.REL_PRZEZ, Key.REL_W, Key.REL_Z, Key.REL_ZA, Key.REL_ZE]

stem_tag_keys = [Key.REL_ANTY, Key.REL_BEZ,
                 Key.REL_DLA, Key.REL_DO, Key.REL_NA, Key.REL_NAD, Key.REL_OD, Key.REL_O, Key.REL_POD, Key.REL_PO,
                 Key.REL_PRZECIW, Key.REL_PRZED, Key.REL_PRZEZ, Key.REL_W, Key.REL_Z, Key.REL_ZA, Key.REL_ZE]
