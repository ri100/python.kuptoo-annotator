from setuptools import setup

setup(
    name='kuptoo_annotator',
    version='0.5',
    description='Product description annotator',
    author='Risto Kowaczewski',
    author_email='risto.kowaczewski@gmail.com',
    packages=['kuptoo_annotator'],
    install_requires=[
        'data-utils @ git+https://ri100@bitbucket.org/ri100/python.dataset.git#egg=data-utils',
        'numpy',
        'tensorflow==2.2.0',
    ],
    include_package_data=True
)
