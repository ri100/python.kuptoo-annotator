import json
import os
from pprint import pprint
from data_utils.nlp.standardizers import StandardizedText
from kuptoo_annotator.model.tagger_model import TaggerModel

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

texts = [
    "talerz płytki",
    "uchwyt napinacza plandeki",
    "ŻWIREK DREWNIANY PODŁOŻE DLA KOTA GRYZONI KRÓLIKA",
    "Pioneer TS-1001i Głośniki z podwójnym stożkiem dopasowane do danego modelu samochodu (110W)",
    "skarpety stopki",
    "stopki damskie",
    "Szelak w płynie do pozłotnictwa Renesans - 250 ml"
]

t = TaggerModel(remapping={'_': "features_1", '=+': "features_2"})
texts = [str(StandardizedText(text.lower()).standardize()) for text in texts]
print(texts)
p = t.predict(texts)
pprint(json.dumps(p))